from django.db import models

class Todo(models.Model):
    title = models.CharField(max_length=30)
    date = models.DateTimeField(auto_now=True)
    author = models.CharField(max_length=30)
    content = models.TextField(max_length=300)

    def __str__(self):
        return self.title