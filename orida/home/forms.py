from django import forms
from .models import Todo

class CreationToDo(forms.ModelForm):
    class Meta:
        model = Todo
        fields = ['author', 'title','content']