from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Todo
from .forms import CreationToDo

@login_required(login_url='loginPage')
def homePage(request):
    form = Todo.objects.filter(author=request.user)
    data = {'form':form}
    return render(request, 'home/home.html', data)

@login_required(login_url='loginPage')
def setToDo(request):
    form = CreationToDo(request.POST)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
    data = {}
    return redirect('homePage')

def delete_ToDo(request, name):
    Todo.objects.filter(author=request.user, title=name).delete()
    return redirect('homePage')