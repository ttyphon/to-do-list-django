from django.urls import path
from . import views

urlpatterns = [
    path('', views.homePage, name='homePage'),
    path('creation-todo', views.setToDo, name='create-todo'),
    path('delete_ToDo/<str:name>', views.delete_ToDo, name='delete-todo'),
]