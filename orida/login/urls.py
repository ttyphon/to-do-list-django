from django.urls import path
from . import views

urlpatterns = [
    path('', views.loginPage, name='loginPage'),
    path('register/', views.register, name='register'),
    path('logout', views.logoutUser, name='logoutPage'),
]