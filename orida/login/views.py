from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout

from django.contrib import messages

from .forms import CreateUserForm

def register(request):
    if request.user.is_authenticated:
        return redirect("homePage")
    else:
        form = CreateUserForm()
        if request.method == "POST":
            form = CreateUserForm(request.POST)
            if form.is_valid():
                print('erez')
                form.save()
                return redirect('loginPage')
        data = {'form':form}
        return render(request, "register/register.html", data)

def loginPage(request):
    if request.user.is_authenticated:
        return redirect("homePage")
    else:
        if request.method == "POST":
            Username = request.POST.get('username')
            Password = request.POST.get('password')
            user = authenticate(request, username=Username, password=Password)
            if user is not None:
                login(request, user)
                return redirect('homePage')
            else:
                messages.info(request, 'Username or Password is incorrect')
        data = {}
        return render(request, 'login/login.html', data)

def logoutUser(request):
    logout(request)
    return redirect('loginPage')
